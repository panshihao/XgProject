<?php
// 设置http header头信息
header("Content-type: text/html;charset=utf-8");

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',True);

// 定义应用目录
define('APP_PATH','./Application/');
require 'ThinkPHP/ThinkPHP.php';
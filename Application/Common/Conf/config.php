<?php
return array (
	// '配置项'=>'配置值'
	'DB_TYPE' => 'mysql', // 数据库类型
	'DB_HOST' => 'localhost', // 服务器地址
	'DB_NAME' => 'xgproject', // 数据库名
	'DB_USER' => 'root', // 用户名
	'DB_PWD' => 'root', // 输入安装MySQL时设置的密码
	'DB_PORT' => '3306', // 端口
	'DB_PREFIX' => 'xg_', // 数据库表前缀
	'DB_CHARSET' => 'utf8',
	'DEFAULT_CHARSET'       =>  'utf-8', // 默认输出编码
);